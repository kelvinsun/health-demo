package demoapp.kelvin.com.health.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class GenoTypeModel {
    @SerializedName("name")
    private String name;
    @SerializedName("symbol")
    private String symbol;

    public GenoTypeModel() {
    }

    public GenoTypeModel(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", symbol='" + symbol + '\'';
    }
}
