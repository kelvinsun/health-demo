package demoapp.kelvin.com.health.network.api;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.utils.Logger;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class APILogout {
    public static synchronized void callApi(@NonNull final Callback<ResponseBody> responseCallback) {
        Call<ResponseBody> call = PNApplication.get().getApiInterface().logout();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                responseCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                responseCallback.onFailure(call, t);
            }
        });
    }
}
