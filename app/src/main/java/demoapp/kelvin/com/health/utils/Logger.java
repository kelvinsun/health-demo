package demoapp.kelvin.com.health.utils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/**
 * Created by kelvinsun on 17/9/2017.
 */

public class Logger {
    private static boolean logEnabled = true;

    private static boolean fullLog = false;

    public static synchronized void setLogEnabled(boolean value) {
        logEnabled = value;
    }

    public static synchronized void setFullLog(boolean value) {
        fullLog = value;
    }

    public static synchronized void log(String value) {
        logInternal(value);
    }

    public static synchronized void log(int value) {
        logInternal(Integer.toString(value));
    }

    public static synchronized void log(float value) {
        logInternal(Float.toString(value));
    }

    public static synchronized void log(double value) {
        logInternal(Double.toString(value));
    }

    public static synchronized void log(long value) {
        logInternal(Long.toString(value));
    }

    public static synchronized void log(char value) {
        logInternal(Character.toString(value));
    }

    public static synchronized void log(boolean value) {
        logInternal(Boolean.toString(value));
    }

    public static synchronized void log(byte value) {
        logInternal(Byte.toString(value));
    }

    public static synchronized void log(StackTraceElement element) {
        Log.d(element.getFileName(), "Line " + element.getLineNumber() + ": " + element.getMethodName() + "()");
    }

    public static synchronized void log(Bundle bundle) {
        for (String key : bundle.keySet()) {
            logInternal(key + ":" + bundle.get(key));
        }
    }

    public static synchronized void fullLog(String value) {
        Logger.setFullLog(true);
        logInternal(value);
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(int value) {
        Logger.setFullLog(true);
        logInternal(Integer.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(float value) {
        Logger.setFullLog(true);
        logInternal(Float.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(double value) {
        Logger.setFullLog(true);
        logInternal(Double.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(long value) {
        Logger.setFullLog(true);
        logInternal(Long.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(char value) {
        Logger.setFullLog(true);
        logInternal(Character.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(boolean value) {
        Logger.setFullLog(true);
        logInternal(Boolean.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullLog(byte value) {
        Logger.setFullLog(true);
        logInternal(Byte.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void warning(String value) {
        warningInternal(value);
    }

    public static synchronized void warning(int value) {
        warningInternal(Integer.toString(value));
    }

    public static synchronized void warning(float value) {
        warningInternal(Float.toString(value));
    }

    public static synchronized void warning(double value) {
        warningInternal(Double.toString(value));
    }

    public static synchronized void warning(long value) {
        warningInternal(Long.toString(value));
    }

    public static synchronized void warning(char value) {
        warningInternal(Character.toString(value));
    }

    public static synchronized void warning(boolean value) {
        warningInternal(Boolean.toString(value));
    }

    public static synchronized void warning(byte value) {
        warningInternal(Byte.toString(value));
    }

    public static synchronized void warning(StackTraceElement element) {
        Log.w(element.getFileName(), "Line " + element.getLineNumber() + ": " + element.getMethodName() + "()");
    }

    public static synchronized void warning(Bundle bundle) {
        for (String key : bundle.keySet()) {
            logInternal(key + ":" + bundle.get(key));
        }
    }

    public static synchronized void fullWarning(String value) {
        Logger.setFullLog(true);
        warningInternal(value);
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(int value) {
        Logger.setFullLog(true);
        warningInternal(Integer.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(float value) {
        Logger.setFullLog(true);
        warningInternal(Float.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(double value) {
        Logger.setFullLog(true);
        warningInternal(Double.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(long value) {
        Logger.setFullLog(true);
        warningInternal(Long.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(char value) {
        Logger.setFullLog(true);
        warningInternal(Character.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(boolean value) {
        Logger.setFullLog(true);
        warningInternal(Boolean.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullWarning(byte value) {
        Logger.setFullLog(true);
        warningInternal(Byte.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void info(String value) {
        infoInternal(value);
    }

    public static synchronized void info(int value) {
        infoInternal(Integer.toString(value));
    }

    public static synchronized void info(float value) {
        infoInternal(Float.toString(value));
    }

    public static synchronized void info(double value) {
        infoInternal(Double.toString(value));
    }

    public static synchronized void info(long value) {
        infoInternal(Long.toString(value));
    }

    public static synchronized void info(char value) {
        infoInternal(Character.toString(value));
    }

    public static synchronized void info(boolean value) {
        infoInternal(Boolean.toString(value));
    }

    public static synchronized void info(byte value) {
        infoInternal(Byte.toString(value));
    }

    public static synchronized void info(StackTraceElement element) {
        Log.i(element.getFileName(), "Line " + element.getLineNumber() + ": " + element.getMethodName() + "()");
    }

    public static synchronized void info(Bundle bundle) {
        for (String key : bundle.keySet()) {
            logInternal(key + ":" + bundle.get(key));
        }
    }

    public static synchronized void fullInfo(String value) {
        Logger.setFullLog(true);
        infoInternal(value);
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(int value) {
        Logger.setFullLog(true);
        infoInternal(Integer.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(float value) {
        Logger.setFullLog(true);
        infoInternal(Float.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(double value) {
        Logger.setFullLog(true);
        infoInternal(Double.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(long value) {
        Logger.setFullLog(true);
        infoInternal(Long.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(char value) {
        Logger.setFullLog(true);
        infoInternal(Character.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(boolean value) {
        Logger.setFullLog(true);
        infoInternal(Boolean.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullInfo(byte value) {
        Logger.setFullLog(true);
        infoInternal(Byte.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void error(String value) {
        errorInternal(value);
    }

    public static synchronized void error(int value) {
        errorInternal(Integer.toString(value));
    }

    public static synchronized void error(float value) {
        errorInternal(Float.toString(value));
    }

    public static synchronized void error(double value) {
        errorInternal(Double.toString(value));
    }

    public static synchronized void error(long value) {
        errorInternal(Long.toString(value));
    }

    public static synchronized void error(char value) {
        errorInternal(Character.toString(value));
    }

    public static synchronized void error(boolean value) {
        errorInternal(Boolean.toString(value));
    }

    public static synchronized void error(byte value) {
        errorInternal(Byte.toString(value));
    }

    public static synchronized void error(final StackTraceElement element) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Log.e(element.getFileName(), "Line " + element.getLineNumber() + ": " + element.getMethodName() + "()");
            }
        });
    }

    public static synchronized void error(Bundle bundle) {
        for (String key : bundle.keySet()) {
            logInternal(key + ":" + bundle.get(key));
        }
    }

    public static synchronized void error(Exception e) {
        if (e != null) {
            errorInternal(e.toString());
        }
    }

    public static synchronized void fullError(String value) {
        Logger.setFullLog(true);
        errorInternal(value);
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(int value) {
        Logger.setFullLog(true);
        errorInternal(Integer.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(float value) {
        Logger.setFullLog(true);
        errorInternal(Float.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(double value) {
        Logger.setFullLog(true);
        errorInternal(Double.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(long value) {
        Logger.setFullLog(true);
        errorInternal(Long.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(char value) {
        Logger.setFullLog(true);
        errorInternal(Character.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(boolean value) {
        Logger.setFullLog(true);
        errorInternal(Boolean.toString(value));
        Logger.setFullLog(false);
    }

    public static synchronized void fullError(byte value) {
        Logger.setFullLog(true);
        errorInternal(Byte.toString(value));
        Logger.setFullLog(false);
    }

    private static synchronized void logInternal(final String message) {
        if (message != null) {
            if (!fullLog) {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                if (stack.length > 4) {
                    final StackTraceElement targetStack = stack[4];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("Logger.java", message);
                        }
                    });
                }
            } else {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                for (int i = 0; i < stack.length; i++) {
                    final StackTraceElement targetStack = stack[i];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                }
            }
        }
    }

    private static synchronized void warningInternal(final String message) {
        if (message != null) {
            if (!fullLog) {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                if (stack.length > 4) {
                    final StackTraceElement targetStack = stack[4];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.w(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.w("Logger.java", message);
                        }
                    });
                }
            } else {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                for (int i = 0; i < stack.length; i++) {
                    final StackTraceElement targetStack = stack[i];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.w(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                }
            }
        }
    }

    private static synchronized void infoInternal(final String message) {
        if (message != null) {
            if (!fullLog) {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                if (stack.length > 4) {
                    final StackTraceElement targetStack = stack[4];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                } else {
                    Log.i("Logger.java", message);
                }
            } else {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                for (int i = 0; i < stack.length; i++) {
                    final StackTraceElement targetStack = stack[i];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                }
            }
        }
    }

    private static synchronized void errorInternal(final String message) {
        if (message != null) {
            if (!fullLog) {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                if (stack.length > 4) {
                    final StackTraceElement targetStack = stack[4];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                } else {
                    Log.e("Logger.java", message);
                }
            } else {
                StackTraceElement[] stack = Thread.currentThread().getStackTrace();
                for (int i = 0; i < stack.length; i++) {
                    final StackTraceElement targetStack = stack[i];
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(targetStack.getFileName(), "Line " + targetStack.getLineNumber() + ": " + targetStack.getMethodName() + "(): " + message);
                        }
                    });
                }
            }
        }
    }
}
