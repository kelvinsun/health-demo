package demoapp.kelvin.com.health.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class BaseActivity extends AppCompatActivity {
    protected String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
