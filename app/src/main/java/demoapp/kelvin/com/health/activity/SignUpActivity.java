package demoapp.kelvin.com.health.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.R;
import demoapp.kelvin.com.health.network.APIUtil;
import demoapp.kelvin.com.health.utils.AppUtil;
import demoapp.kelvin.com.health.utils.ViewUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class SignUpActivity extends BaseActivity {

    // UI references.
    private AutoCompleteTextView mFirstNameView;
    private AutoCompleteTextView mLastNameView;
    private AutoCompleteTextView mDOBView;
    private AutoCompleteTextView mEmailView;
    private View mProgressView;
    private View mRegisterFormView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mProgressView = ViewUtil.findViewById(this,R.id.register_progress);
        mRegisterFormView = ViewUtil.findViewById(this,R.id.register_form);
        mFirstNameView = ViewUtil.findViewById(this,R.id.first_name);
        mLastNameView = ViewUtil.findViewById(this,R.id.last_name);
        mEmailView = ViewUtil.findViewById(this,R.id.email);
        mDOBView = ViewUtil.findViewById(this,R.id.dob);

        Button mRegisterButton = ViewUtil.findViewById(this,R.id.register_button);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerAccount();
            }
        });

        initView();
    }

    private void initView() {

    }

    private void registerAccount() {
        // Reset errors.
        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mDOBView.setError(null);


        // Store values at the time of the login attempt.
        final String firstName = mFirstNameView.getText().toString();
        final String lastName = mLastNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid first name, if the user entered one.
        if (!TextUtils.isEmpty(firstName)) {
            mFirstNameView.setError(getString(R.string.error_field_required));
            focusView = mFirstNameView;
            cancel = true;
        }

        // Check for a valid last name.
        if (TextUtils.isEmpty(lastName)) {
            mLastNameView.setError(getString(R.string.error_field_required));
            focusView = mLastNameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            //TODO:register api call
            APIUtil.callLogin(firstName, lastName, new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    showProgress(false);
                    //save userSession
                    PNApplication.get().getSession().setFirstName(firstName);
                    PNApplication.get().getSession().setLastName(lastName);
                    PNApplication.get().saveSessionDataInPreference();
                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showProgress(false);
                    if (t!=null) {
                        String errorMsg=t.getMessage();
                        AppUtil.showToast(SignUpActivity.this,errorMsg);
                    }
//                    mPasswordView.setError(t.getMessage());
//                    mPasswordView.requestFocus();
                }
            });
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}
