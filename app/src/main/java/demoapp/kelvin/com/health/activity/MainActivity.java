package demoapp.kelvin.com.health.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.R;
import demoapp.kelvin.com.health.UserSession;
import demoapp.kelvin.com.health.network.APIUtil;
import demoapp.kelvin.com.health.network.api.APIUserGenoType;
import demoapp.kelvin.com.health.network.model.GenoTypeModel;
import demoapp.kelvin.com.health.utils.AppUtil;
import demoapp.kelvin.com.health.utils.StringUtil;
import demoapp.kelvin.com.health.utils.ViewUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinsun on 12/1/2018.
 */

public class MainActivity extends BaseActivity {

    private TextView mUserName;
    private TextView mEmail;
    private TextView mDOB;
    private TextView mGenotype;

    private Button mFetchUserGenotypeButton;
    private Button mMeasureHeartRateButton;
    private Button mLogoutButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUserName = ViewUtil.findViewById(this, R.id.tv_name);
        mEmail = ViewUtil.findViewById(this, R.id.tv_email);
        mDOB = ViewUtil.findViewById(this, R.id.tv_dob);
        mGenotype = ViewUtil.findViewById(this, R.id.tv_symbol);

        mFetchUserGenotypeButton = ViewUtil.findViewById(this, R.id.bn_fetch_user_genotype);
        mMeasureHeartRateButton = ViewUtil.findViewById(this, R.id.bn_measure_heart_rate);
        mLogoutButton = ViewUtil.findViewById(this, R.id.bn_logout);

        initView();
        addListener();
    }

    private void initView() {
        UserSession userSession = PNApplication.get().getSession();

        String userName = "-";
        if (!StringUtil.isNullOrEmpty(userSession.getUserName())) {
            userName = userSession.getUserName();
        }
        mUserName.setText(userName);

        String email = "-";
        if (!StringUtil.isNullOrEmpty(userSession.getEmail())) {
            email = userSession.getEmail();
        }
        mEmail.setText(email);

        String dob = "-";
        if (!StringUtil.isNullOrEmpty(userSession.getDob())) {
            dob = userSession.getDob();
        }
        mDOB.setText(dob);

        String genotype = "-";
        if (!StringUtil.isNullOrEmpty(userSession.getGenotype())) {
            genotype = userSession.getGenotype();
        }
        mGenotype.setText(genotype);
    }

    private void addListener() {
        mFetchUserGenotypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFetchUserGenotype();
            }
        });

        mMeasureHeartRateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMeasureHeartRateActivity();
            }
        });

        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void mFetchUserGenotype() {
        //genotype api call
        APIUtil.callUserGenoType(new APIUserGenoType.APICallBack() {
            @Override
            public void onSuccess(List<GenoTypeModel> genoTypeModelList) {
                AppUtil.showToast(MainActivity.this, "GenoType updated");
                //update UI
                String genoType;
                if (genoTypeModelList != null && genoTypeModelList.size()>0) {
                    genoType = genoTypeModelList.toString();
                }else{
                    genoType="-";
                }
                PNApplication.get().getSession().setGenotype(genoType);
                PNApplication.get().saveSessionDataInPreference();
                mGenotype.setText(genoType);
            }

            @Override
            public void onFailure(String message) {
                AppUtil.showToast(MainActivity.this, message);
            }
        });
    }

    private void openMeasureHeartRateActivity() {
        Intent intent = new Intent(MainActivity.this, HeartRateMonitorActivity.class);
        startActivity(intent);
    }

    private void logout() {
        APIUtil.callLogout(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                PNApplication.get().clearSessionDataInPreference();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t != null) {
                    String errorMsg = t.getMessage();
                    AppUtil.showToast(MainActivity.this, errorMsg);
                }
            }
        });

    }
}
