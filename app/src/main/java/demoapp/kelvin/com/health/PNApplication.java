package demoapp.kelvin.com.health;

import android.app.Application;
import android.text.TextUtils;

import com.securepreferences.SecurePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import demoapp.kelvin.com.health.network.APIInterface;
import demoapp.kelvin.com.health.network.ServiceGenerator;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class PNApplication extends Application {

    private static WeakReference<PNApplication> mAppInstance;
    private static final String SESSION = "s";

    private APIInterface apiInterface;
    private UserSession mSession;

    @Override
    public void onCreate() {
        super.onCreate();

        if (mAppInstance == null) {
            // create short hand
            mAppInstance = new WeakReference<>(this);
        }

        // create UserSession
        UserSession savedSession = restoreSessionInPreference();

        initApiService();

        if (savedSession != null) {
            mSession = savedSession;
        } else {
            mSession = new UserSession.Builder()
                    .createSession();
        }
    }

    public static PNApplication get() {
        return mAppInstance.get();
    }

    public UserSession restoreSessionInPreference() {
        String string = getSecurePrefs().getString(SESSION, "");
        if (!TextUtils.isEmpty(string)) {
            // restore
            try {
                JSONObject sessionObject = new JSONObject(string);
                return new UserSession(sessionObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public SecurePreferences getSecurePrefs() {
        return new SecurePreferences(this);
    }

    public SecurePreferences.Editor getSecurePrefsEditor() {
        SecurePreferences securePrefs = new SecurePreferences(this);
        return securePrefs.edit();
    }

    public UserSession getSession() {
        return mSession;
    }

    public void saveSessionDataInPreference() {
        JSONObject sessionObject = getSession().toJSONObject();
        if (sessionObject != null) {
            getSecurePrefsEditor().putString(SESSION, sessionObject.toString()).apply();
        }
    }

    public void clearSessionDataInPreference() {
        getSecurePrefsEditor().clear().commit();
        mSession = new UserSession.Builder().createSession();
    }

    public void initApiService() {
        apiInterface = ServiceGenerator.createService(APIInterface.class);
    }

    public APIInterface getApiInterface() {
        return apiInterface;
    }
}
