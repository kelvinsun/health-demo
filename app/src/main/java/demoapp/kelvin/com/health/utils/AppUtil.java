package demoapp.kelvin.com.health.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.DrawableRes;
import android.text.format.DateFormat;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class AppUtil {
    /**
     * General message show in toast
     *
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        if (context == null) return;
        if (!StringUtil.isNullOrEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static long getCurrentTime(){
        return new Date().getTime();
    }

    public static String getCurrentTimeString(){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(getCurrentTime());
        String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
        return date;
    }

    public static Bitmap convertDrawableResToBitmap(Context context,@DrawableRes int drawableId, Integer width, Integer height) {
        Drawable d = context.getResources().getDrawable(drawableId);

        if (d instanceof BitmapDrawable) {
            return ((BitmapDrawable) d).getBitmap();
        }

        if (d instanceof GradientDrawable) {
            GradientDrawable g = (GradientDrawable) d;

            int w = d.getIntrinsicWidth() > 0 ? d.getIntrinsicWidth() : width;
            int h = d.getIntrinsicHeight() > 0 ? d.getIntrinsicHeight() : height;

            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            g.setBounds(0, 0, w, h);
            g.setStroke(1, Color.BLACK);
            g.setFilterBitmap(true);
            g.draw(canvas);
            return bitmap;
        }

        Bitmap bit = BitmapFactory.decodeResource(context.getResources(), drawableId);
        return bit.copy(Bitmap.Config.ARGB_8888, true);
    }

}
