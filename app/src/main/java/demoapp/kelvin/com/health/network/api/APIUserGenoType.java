package demoapp.kelvin.com.health.network.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.UserSession;
import demoapp.kelvin.com.health.network.model.GenoTypeModel;
import demoapp.kelvin.com.health.utils.Logger;
import demoapp.kelvin.com.health.utils.StringUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class APIUserGenoType {
    public static synchronized void callApi(@NonNull final APICallBack responseCallback) {
        UserSession userSession = PNApplication.get().getSession();
        Call<ResponseBody> call = PNApplication.get().getApiInterface().getUserGeneticResult(userSession.getCustomerID());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:

                        String contentBody = response.body().toString();
                        Gson gson = new GsonBuilder().create();
                        List<GenoTypeModel> genoTypeModelList = new ArrayList<GenoTypeModel>();
                        try {
                            genoTypeModelList = gson.fromJson(contentBody, new TypeToken<List<GenoTypeModel>>() {
                            }.getType());
                        } catch (JsonSyntaxException e) {
                            responseCallback.onSuccess(genoTypeModelList);
                        }

                        responseCallback.onSuccess(genoTypeModelList);

                        break;
                    default:
                    case 400:
                    case 401:
                    case 403:
                        Throwable t;
                        try {
                            t = new Throwable(response.message() + "(" + response.code() + ")");
                            responseCallback.onFailure(t.getMessage());
                        } catch (NullPointerException e) {
                            responseCallback.onFailure("Error");
                        }
                        break;

                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                responseCallback.onFailure("Error");
            }
        });
    }

    public interface APICallBack {
        void onSuccess(List<GenoTypeModel> genoTypeModelList);

        void onFailure(String message);
    }
}
