package demoapp.kelvin.com.health.network.api;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.utils.Logger;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class APILogin {
    public static synchronized void callApi(String userName, String password, @NonNull final Callback<ResponseBody> responseCallback) {
        final Request request = new Request(userName, password);
        Call<ResponseBody> call = PNApplication.get().getApiInterface().login(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        responseCallback.onResponse(call, response);
                        break;
                    default:
                    case 401:
                        Throwable t;
                        try {
                            t = new Throwable(response.message() + "(" + response.code() + ")");
                            responseCallback.onFailure(call, t);
                        } catch (NullPointerException e) {
                            t = new Throwable("Error");
                            responseCallback.onFailure(call, t);
                        }
                        break;

                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                responseCallback.onFailure(call, t);
            }
        });
    }

    public static class Request {
        @SerializedName("username")
        protected String userName;

        @SerializedName("password")
        protected String password;

        public Request(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }
    }
}
