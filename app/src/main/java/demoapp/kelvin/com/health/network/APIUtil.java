package demoapp.kelvin.com.health.network;

import android.support.annotation.Nullable;

import java.util.List;

import demoapp.kelvin.com.health.network.api.APILogin;
import demoapp.kelvin.com.health.network.api.APILogout;
import demoapp.kelvin.com.health.network.api.APISubmitHeartRate;
import demoapp.kelvin.com.health.network.api.APIUserGenoType;
import demoapp.kelvin.com.health.network.model.GenoTypeModel;
import demoapp.kelvin.com.health.utils.AppUtil;
import demoapp.kelvin.com.health.utils.Logger;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class APIUtil {
    public static synchronized void callLogin(String userName, String password, @Nullable final Callback<ResponseBody> responseCallback) {
        APILogin.callApi(userName, password, new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                Logger.info("responseCallback: " + responseCallback);
                if (responseCallback != null) {
                    responseCallback.onResponse(call, response);
                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                Logger.info("responseCallback: " + call);
                if (responseCallback != null) {
                    responseCallback.onFailure(call, t);
                }
            }
        });
    }

    public static synchronized void callLogout(@Nullable final Callback<ResponseBody> responseCallback) {
        APILogout.callApi(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                Logger.info("responseCallback: " + response.toString());
                if (responseCallback != null) {
                    responseCallback.onResponse(call, response);
                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                Logger.info("responseCallback: " + call);
                if (responseCallback != null) {
                    responseCallback.onFailure(call, t);
                }
            }
        });
    }

    public static synchronized void callSubmitHeartRate(String heartRate, String timestamp, @Nullable final Callback<ResponseBody> responseCallback) {
        APISubmitHeartRate.callApi(heartRate, timestamp, new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final Response<ResponseBody> response) {
                Logger.info("responseCallback: " + response.toString());
                if (responseCallback != null) {
                    responseCallback.onResponse(call, response);
                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                Logger.info("responseCallback: " + call);
                if (responseCallback != null) {
                    responseCallback.onFailure(call, t);
                }
            }
        });
    }

    public static synchronized void callUserGenoType(@Nullable final APIUserGenoType.APICallBack responseCallback) {
        APIUserGenoType.callApi(new APIUserGenoType.APICallBack() {
            @Override
            public void onSuccess(List<GenoTypeModel> genoTypeModelList) {
                Logger.info("responseCallback: " + genoTypeModelList.toString());
                if (responseCallback != null) {
                    responseCallback.onSuccess(genoTypeModelList);
                }
            }

            @Override
            public void onFailure(String message) {
                Logger.info("responseCallback: " + message);
                responseCallback.onFailure(message);

            }

        });
    }

}
