package demoapp.kelvin.com.health.network;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import demoapp.kelvin.com.health.utils.Logger;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by kelvinsun on 17/9/2017.
 */

public class ServiceGenerator {

    public static String API_BASE_URL = "https://api.prenetics.com/";

    public static String API_BASE_URL_PATH = "v1/";

    private static OkHttpClient.Builder httpClient;

    private static Retrofit.Builder builder;

    private static Retrofit retrofit;

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);
    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {

        //init http client
        httpClient = new OkHttpClient.Builder()
                //bypass the Err: java.security.cert.CertPathValidatorException
                .hostnameVerifier(new NullHostNameVerifier())
                .sslSocketFactory(getSSLSocketFactory());

        //enable API logging
        HttpLoggingInterceptor loggerInterceptor = new HttpLoggingInterceptor();
        loggerInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggerInterceptor);

        try {
            builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
            builder.client(httpClient.build());
            retrofit = builder.build();
        } catch (IllegalArgumentException e) {
            Logger.error(e.toString());
        }

        return retrofit.create(serviceClass);
    }

    private static String formatHostUrl(String hostUrl) {
        if (hostUrl.endsWith("/")) {
            return hostUrl;
        } else {
            return hostUrl + "/";
        }
    }

    private static SSLSocketFactory getSSLSocketFactory() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return sslSocketFactory;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static class NullHostNameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            Logger.info("RestUtilImpl: Approving certificate for " + hostname);
            return true;
        }

    }

}