package demoapp.kelvin.com.health.network;

/**
 * Created by kelvinsun on 15/1/2018.
 */

import java.util.List;

import demoapp.kelvin.com.health.network.api.APILogin;
import demoapp.kelvin.com.health.network.api.APISubmitHeartRate;
import demoapp.kelvin.com.health.network.model.GenoTypeModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {
    @POST("v1/customer/login")
    Call<ResponseBody> login(@Body APILogin.Request request);

    @POST("v1/customer/logout")
    Call<ResponseBody> logout();

    @GET("v1/customer/{customerId}/user")
    Call<ResponseBody> getUserProfile(@Path("customerId") int customerId);

    @GET("v1/customer/{customerId}/genetic")
    Call<ResponseBody> getUserGeneticResult(@Path("customerId") int customerId);

    @POST("v1/customer/{customerId}/heartrate")
    Call<ResponseBody> submitHeartRate(@Path("customerId") int customerId,@Body APISubmitHeartRate.Request request);


}
