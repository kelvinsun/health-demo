package demoapp.kelvin.com.health.network.api;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import demoapp.kelvin.com.health.PNApplication;
import demoapp.kelvin.com.health.utils.Logger;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class APISubmitHeartRate {
    public static synchronized void callApi(String rate, String timestamp, @NonNull final Callback<ResponseBody> responseCallback) {
        final Request request = new Request(rate, timestamp);
        int customerID = PNApplication.get().getSession().getCustomerID();
        Call<ResponseBody> call = PNApplication.get().getApiInterface().submitHeartRate(customerID, request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        responseCallback.onResponse(call, response);
                        break;
                    default:
                    case 400:
                    case 401:
                    case 403:
                        Throwable t;
                        try {
                            t = new Throwable(response.message() + "(" + response.code() + ")");
                            responseCallback.onFailure(call, t);
                        } catch (NullPointerException e) {
                            t = new Throwable("Error");
                            responseCallback.onFailure(call, t);
                        }
                        break;

                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, final Throwable t) {
                responseCallback.onFailure(call, t);
            }
        });
    }

    public static class Request {
        @SerializedName("rate")
        protected String rate;

        @SerializedName("timestamp")
        protected String timestamp;

        public Request(String rate, String timestamp) {
            this.rate = rate;
            this.timestamp = timestamp;
        }
    }
}
