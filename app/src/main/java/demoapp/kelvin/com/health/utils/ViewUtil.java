package demoapp.kelvin.com.health.utils;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class ViewUtil {
    @SuppressWarnings("unchecked")
    @Nullable
    public static synchronized <T extends View> T findViewById(@NonNull View parent, @IdRes int id) {
        return (T) parent.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static synchronized <T extends View> T findViewById(@NonNull View parent, @IdRes int... id) {
        View view = null;
        if (id.length > 0) {
            view = parent;
            for (int i = 0; i < id.length; i++) {
                view = view.findViewById(id[i]);
            }
        }
        return (T) view;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static synchronized <T extends View> T findViewById(@NonNull Activity activity, @IdRes int id) {
        return (T) activity.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static synchronized <T extends View> T findViewById(@NonNull Activity activity, @IdRes int... id) {
        View view = null;
        if (id.length > 0) {
            view = activity.findViewById(id[0]);
            for (int i = 0; i < id.length; i++) {
                if (view == null) {
                    break;
                }
                view = view.findViewById(id[i]);
            }
        }
        return (T) view;
    }
}
