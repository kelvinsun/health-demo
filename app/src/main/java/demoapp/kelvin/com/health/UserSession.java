package demoapp.kelvin.com.health;

import org.json.JSONException;
import org.json.JSONObject;

import demoapp.kelvin.com.health.utils.StringUtil;

/**
 * Created by kelvinsun on 15/1/2018.
 */

public class UserSession {

    /**
     * user info
     */
    private int customerID;
    private String firstName;
    private String lastName;
    private String email;
    private String dob;
    private String userName;
    private String password;
    private String genotype;

    public UserSession(JSONObject object) {
        this.customerID =object.optInt("customer_id");
        this.firstName = object.optString("first_name");
        this.lastName = object.optString("last_name");
        this.email = object.optString("email");
        this.dob = object.optString("dob");
        this.userName = object.optString("username");
        this.password = object.optString("password");
        this.genotype = object.optString("genotype");
    }

    public UserSession(int customerID, String firstName, String lastName, String email,
                       String dob, String userName, String password, String genotype) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.userName = userName;
        this.password = password;
        this.genotype = genotype;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGenotype() {
        return genotype;
    }

    public void setGenotype(String genotype) {
        this.genotype = genotype;
    }

    public JSONObject toJSONObject() {

        JSONObject object = new JSONObject();
        try {
            if (customerID>0) {
                object.put("customer_id", customerID);
            }


            if (!StringUtil.isNullOrEmpty(firstName)) {
                object.put("first_name", firstName);
            }


            if (!StringUtil.isNullOrEmpty(lastName)) {
                object.put("last_name", lastName);
            }

            if (!StringUtil.isNullOrEmpty(password)) {
                object.put("password", password);
            }

            if (!StringUtil.isNullOrEmpty(email)) {
                object.put("email", email);
            }

            if (!StringUtil.isNullOrEmpty(dob)) {
                object.put("dob", dob);
            }

            if (!StringUtil.isNullOrEmpty(userName)) {
                object.put("username", userName);
            }

            if (!StringUtil.isNullOrEmpty(password)) {
                object.put("password", password);
            }

            if (!StringUtil.isNullOrEmpty(genotype)) {
                object.put("genotype", genotype);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return object;
    }

    /**
     * Session Builder
     */
    public static class Builder {
        private int mNestedCustomerID;
        private String mNestedFirstName;
        private String mNestedLastName;
        private String mNestedEmail;
        private String mNestedDOB;
        private String mNestedUserName;
        private String mNestedPassword;
        private String mNestedGenotype;

        public Builder customerID(int customerID) {
            this.mNestedCustomerID = customerID;
            return this;
        }

        public Builder firstName(String firstName) {
            this.mNestedFirstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.mNestedLastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.mNestedEmail = email;
            return this;
        }

        public Builder dob(String dob) {
            this.mNestedDOB = dob;
            return this;
        }

        public Builder username(String username) {
            this.mNestedUserName = username;
            return this;
        }

        public Builder password(String password) {
            this.mNestedPassword = password;
            return this;
        }

        public Builder genotype(String genotype) {
            this.mNestedGenotype = genotype;
            return this;
        }

        public UserSession createSession() {
            return new UserSession(
                    mNestedCustomerID,
                    mNestedFirstName,
                    mNestedLastName,
                    mNestedEmail,
                    mNestedDOB,
                    mNestedUserName,
                    mNestedPassword,
                    mNestedGenotype);
        }
    }
}
