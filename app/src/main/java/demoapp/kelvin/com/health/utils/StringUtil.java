package demoapp.kelvin.com.health.utils;

/**
 * Created by kelvinsun on 16/9/2017.
 */

public class StringUtil {
    public static synchronized boolean isNullOrEmpty(final String check) {
        if (check == null) {
            return true;
        }
        if (check.isEmpty()) {
            return true;
        }
        return false;
    }

}
